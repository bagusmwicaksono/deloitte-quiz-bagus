
# DeloitteQuizBagus

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.2.2.
Hi, my name is @bagusmwicaksono and this is my implementation of single page application (SPA) to display map of countries based on [This API](https://restcountries.eu).

## Technical Aspect

 - Angular Framework from [Angular.io](https://angular.io/)
 - Angular Material from [Angular Material](https://material.angular.io/)
 - Google Maps for Angular [AGM](https://angular-maps.com/)

## Getting Started

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Live Demo

Please access here [Live Demo](http://ec2-54-179-189-131.ap-southeast-1.compute.amazonaws.com:3000) to see how the application working.

## Features
- [DONE] The SPA will allow the user to select a Regional Bloc and will list the names of countries belonging to it in the left column.
- [DONE] Clicking the country name will display the information on the right column using a static map.
- [DONE] The marker should be the first letter of the country name,
- [DONE] The default selection will be the first options in the drop downs and the first country on the list.
- [DONE] Clicking the language will change the name of the countries corresponding to the language.
- [DONE] Back button support
- [DONE] Data persistence (state will persist even on page refresh)
- [DONE] Paging in country list (BONUS)
- [PENDING] Validation in user input (Not really necessary since user only select from dropdown input)
- [PENDING] Mobile browser support (Nice to have, will update later)
- [PENDING] Error handling: Not yet completed will enhanced for next iteration 

## Further help
Contact me by email bagusmwicaksono@gmail.com
[Bitbucket Repositories](https://bitbucket.org/bagusmwicaksono/deloitte-quiz-bagus)

