import { NgModule } from '@angular/core';
import { 
  MatFormFieldModule,
  MatInputModule,
  MatSelectModule,
  MatButtonModule,
  MatTableModule,
  MatToolbarModule,
  MatPaginatorModule
 } from "@angular/material";
 import { FlexLayoutModule } from "@angular/flex-layout";
 import { ReactiveFormsModule } from "@angular/forms";
 import { AgmCoreModule  } from "@agm/core";



@NgModule({
  declarations: [],
  imports: [
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatButtonModule,
    MatTableModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCfQyBqR8H2Nh_SqCnTSO3cR1m-NtdyNaY'
    }),
    MatToolbarModule,
    MatPaginatorModule
  ],
  exports:[
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatButtonModule,
    MatTableModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    AgmCoreModule,
    MatToolbarModule,
    MatPaginatorModule
  ]
})
export class MaterialModule { }
