import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Countries } from './countries';

@Injectable({
  providedIn: 'root'
})
export class CountriesService { 
  constructor(private http: HttpClient) { }  
  getCountries(blockCode,langCode){
    if(blockCode){
      return this.http.get<Countries[]>("https://restcountries.eu/rest/v2/regionalbloc/"+blockCode);
    }    
  }
}
