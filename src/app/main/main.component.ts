import { Component, OnInit, ViewChild } from '@angular/core';
import { CountriesService } from '../countries.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { Countries } from '../countries';

export interface selectData{
  code: String;
  name: String;
}

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})

export class MainComponent implements OnInit {  
  form: FormGroup=new FormGroup({
    blocCode: new FormControl('EU',Validators.required),
    langCode: new FormControl('EN',Validators.required)
  });  
  myBlocData: selectData[]=[
    {code: 'EU', name: 'EU (European Union)'},
    {code: 'EFTA', name: 'EFTA (European Free Trade Association)'},
    {code: 'CARICOM', name: 'CARICOM (Caribbean Community)'},
    {code: 'PA', name: 'PA (Pacific Alliance)'},
    {code: 'AU', name: 'AU (African Union)'},
    {code: 'USAN', name: 'USAN (Union of South American Nations)'},
    {code: 'EEU', name: 'EEU (Eurasian Economic Union)'},
    {code: 'AL', name: 'AL (Arab League)'},
    {code: 'ASEAN', name: 'ASEAN (Association of Southeast Asian Nations)'},
    {code: 'CAIS', name: 'CAIS (Central American Integration System)'},
    {code: 'CEFTA', name: 'CEFTA (Central European Free Trade Agreement)'},
    {code: 'NAFTA', name: 'NAFTA (North American Free Trade Agreement)'},
    {code: 'SAARC', name: 'SAARC (South Asian Association for Regional Cooperation)'},
  ];
  lang: selectData[]=[
    {code: 'EN', name: 'EN (English)'},
    {code: 'DE', name: 'DE (Germany)'},
    {code: 'ES', name: 'ES (Spain)'},
    {code: 'FR', name: 'FR (France)'},
    {code: 'JA', name: 'JA (Japan)'},
    {code: 'IT', name: 'IT (Italy)'},
    {code: 'BR', name: 'BR (Brazil)'},
    {code: 'PT', name: 'PT (Portugal)'}
  ]
  constructor(public service: CountriesService,private router: Router,private route: ActivatedRoute) {
    let bcode=this.route.snapshot.paramMap.get("blocCode");
    if(!bcode){
      localStorage.clear();
      this.form.setValue({
        blocCode: 'EU',
        langCode: 'EN'
      });
      this.onChange();
    }else{
      this.route.params.subscribe(params=>{
        this.changeBlocValue(params.blocCode,params.langCode);            
        this.service.getCountries(params.blocCode,params.langCode).subscribe(countries => {
          this.dataSource=new MatTableDataSource<Countries>(countries);
          this.dataSource.paginator = this.paginator;
          if(params.countryCode){
            let selectedCountry = countries.filter(function(country) {
                return country.alpha2Code === params.countryCode;
            })[0];          
            this.retrieveCountryDetail(selectedCountry); 
          }else{
            this.retrieveCountryDetail(countries[0]); //Default use the first country
          }                             
        });        
        this.selectedBloc=params.blocCode;
        this.selectedLang=params.langCode;
      });
    }    
   }

  getRecord(row){
    console
    if(this.form.valid){
      this.router.navigate(['/main/'+this.form.value.blocCode+'/'+this.form.value.langCode+'/'+row.alpha2Code]);
    } 
  }
  retrieveCountryDetail(selectedCountry){
    this.selectedCountry=selectedCountry;
    this.lat=selectedCountry.latlng[0];
    this.lng=selectedCountry.latlng[1];
    this.mylabel=selectedCountry.name.charAt(0);
    switch(this.selectedLang){
      case 'EN':{
        this.localizedCountryName=selectedCountry.name;
        break;
      }
      case 'DE':{
        this.localizedCountryName=selectedCountry.translations.de;
        break;
      }
      case 'ES':{
        this.localizedCountryName=selectedCountry.translations.es;
        break;
      }
      case 'FR':{
        this.localizedCountryName=selectedCountry.translations.fr;
        break;
      }
      case 'JA':{
        this.localizedCountryName=selectedCountry.translations.ja;
        break;
      }
      case 'IT':{
        this.localizedCountryName=selectedCountry.translations.it;
        break;
      }
      case 'BR':{
        this.localizedCountryName=selectedCountry.translations.br;
        break;
      }
      case 'PT':{
        this.localizedCountryName=selectedCountry.translations.pt;
        break;
      }      
    }
  }
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  ngOnInit() {
  }
  displayedColumns: string[] = ['name'];
  dataSource: MatTableDataSource<Countries>;
  selectedCountry;
  selectedBloc;
  selectedLang;
  localizedCountryName;
  lat;
  lng;
  mylabel;
  
  zoom: number = 5;
  onChange(){
    if(this.selectedCountry){
      this.router.navigate(['/main/'+this.form.value.blocCode+'/'+this.form.value.langCode+'/'+this.selectedCountry.alpha2Code]);
    }else{
      this.router.navigate(['/main/'+this.form.value.blocCode+'/'+this.form.value.langCode]);
    }  
  }
  onChangeBloc(){
    this.router.navigate(['/main/'+this.form.value.blocCode+'/'+this.form.value.langCode]);  
  }
  public changeBlocValue(blocCode,langCode){
    this.form.patchValue({
      blocCode: blocCode,
      langCode:  langCode
    });  
  }  
}
